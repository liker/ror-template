RAILS_REQUIREMENT = "~> 5.2.0".freeze

def apply_template!
  assert_minimum_rails_version
  add_template_repository_to_source_path

  template "Gemfile.tt", force: true

#  template "README.md.tt", force: true
  remove_file "README.rdoc"

  copy_file "gitignore", ".gitignore", force: true

  apply "app/template.rb"
  apply "config/template.rb"

  git :init
#  git remote:  
#  unless any_local_git_commits?
  git add: "."
#    git commit: "-m 'Set up project'"
#  end
end

require "fileutils"
require "shellwords"

# Add this template directory to source_paths so that Thor actions like
# copy_file and template resolve against our source files. If this file was
# invoked remotely via HTTP, that means the files are not present locally.
# In that case, use `git clone` to download them to a local temporary dir.

def add_template_repository_to_source_path
  if __FILE__ =~ %r{\Ahttps?://}
    require "tmpdir"
    source_paths.unshift(tempdir = Dir.mktmpdir("rails-template-"))
    at_exit { FileUtils.remove_entry(tempdir) }
    git clone: [
      "--quiet",
      "https://gitlab.com/liker/ror-template.git",
      tempdir
    ].map(&:shellescape).join(" ")

    if (branch = __FILE__[%r{rails-template/(.+)/template.rb}, 1])
      Dir.chdir(tempdir) { git checkout: branch }
    end
  else
    source_paths.unshift(File.dirname(__FILE__))
  end
end

def assert_minimum_rails_version
  requirement = Gem::Requirement.new(RAILS_REQUIREMENT)
  rails_version = Gem::Version.new(Rails::VERSION::STRING)
  return if requirement.satisfied_by?(rails_version)

  prompt = "This template requires Rails #{RAILS_REQUIREMENT}. "\
           "You are using #{rails_version}. Continue anyway?"
  exit 1 if no?(prompt)
end


def any_local_git_commits?
  system("git log &> /dev/null")
end

apply_template!
