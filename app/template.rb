copy_file "app/controllers/home_controller.rb"
copy_file "app/views/home/index.html.haml"
template "app/views/layouts/application.html.haml.tt"
template "app/views/layouts/_footer.html.haml.tt"
template "app/views/layouts/_header.html.haml.tt"
template "app/views/layouts/mailer.html.haml.tt"
template "app/views/layouts/mailer.text.erb.tt"
template "app/views/layouts/_shim.html.haml.tt"
template "app/helpers/application_helper.rb.tt" , force: true
remove_file "app/views/layouts/application.html.erb"

#bootstrap 4
remove_file "app/assets/stylesheets/application.css"
copy_file "app/assets/stylesheets/application.scss"
copy_file "app/assets/stylesheets/custom.scss"
copy_file "app/assets/javascripts/application.js", force: true

